# NOTE: this definition file depends on features only available in
# Singularity 3.2 and later.
BootStrap: docker
From: nvidia/cuda:10.1-devel-ubuntu18.04
Stage: build
%post
    . /.singularity.d/env/10-docker*.sh

sed -i 's#archive.ubuntu.com#free.nchc.org.tw#' /etc/apt/sources.list

# GNU compiler
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        g++ \
        gcc \
        gfortran
    rm -rf /var/lib/apt/lists/*

# Mellanox OFED version 4.7-3.2.9.0
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ca-certificates \
        gnupg \
        wget
    rm -rf /var/lib/apt/lists/*
%post
    wget -qO - https://www.mellanox.com/downloads/ofed/RPM-GPG-KEY-Mellanox | apt-key add -
    mkdir -p /etc/apt/sources.list.d && wget -q -nc --no-check-certificate -P /etc/apt/sources.list.d https://linux.mellanox.com/public/repo/mlnx_ofed/4.7-3.2.9.0/ubuntu18.04/mellanox_mlnx_ofed.list
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ibverbs-utils \
        libibmad \
        libibmad-devel \
        libibumad \
        libibumad-devel \
        libibverbs-dev \
        libibverbs1 \
        libmlx4-1 \
        libmlx4-dev \
        libmlx5-1 \
        libmlx5-dev \
        librdmacm-dev \
        librdmacm1
    rm -rf /var/lib/apt/lists/*

# GDRCOPY version 2.0
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        make \
        wget
    rm -rf /var/lib/apt/lists/*
%post
    cd /
    mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://github.com/NVIDIA/gdrcopy/archive/v2.0.tar.gz
    mkdir -p /var/tmp && tar -x -f /var/tmp/v2.0.tar.gz -C /var/tmp -z
    cd /var/tmp/gdrcopy-2.0
    mkdir -p /usr/local/gdrcopy/include /usr/local/gdrcopy/lib64
    make PREFIX=/usr/local/gdrcopy lib lib_install
    echo "/usr/local/gdrcopy/lib64" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
    rm -rf /var/tmp/gdrcopy-2.0 /var/tmp/v2.0.tar.gz
%environment
    export CPATH=/usr/local/gdrcopy/include:$CPATH
    export LIBRARY_PATH=/usr/local/gdrcopy/lib64:$LIBRARY_PATH
%post
    export CPATH=/usr/local/gdrcopy/include:$CPATH
    export LIBRARY_PATH=/usr/local/gdrcopy/lib64:$LIBRARY_PATH

# KNEM version 1.1.3
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ca-certificates \
        git
    rm -rf /var/lib/apt/lists/*
%post
    cd /
    mkdir -p /var/tmp && cd /var/tmp && git clone --depth=1 --branch knem-1.1.3 https://gforge.inria.fr/git/knem/knem.git knem && cd -
    mkdir -p /usr/local/knem
    cd /var/tmp/knem
    mkdir -p /usr/local/knem/include
    cp common/*.h /usr/local/knem/include
    echo "/usr/local/knem/lib" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
    rm -rf /var/tmp/knem
%environment
    export CPATH=/usr/local/knem/include:$CPATH
%post
    export CPATH=/usr/local/knem/include:$CPATH

# UCX version 1.8.0
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        binutils-dev \
        file \
        libnuma-dev \
        make \
        wget
    rm -rf /var/lib/apt/lists/*
%post
    cd /
    mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://github.com/openucx/ucx/releases/download/v1.8.0/ucx-1.8.0.tar.gz
    mkdir -p /var/tmp && tar -x -f /var/tmp/ucx-1.8.0.tar.gz -C /var/tmp -z
    cd /var/tmp/ucx-1.8.0 &&   ./configure --prefix=/usr/local/ucx --disable-assertions --disable-debug --disable-doxygen-doc --disable-logging --disable-params-check --enable-devel-headers --enable-optimizations --with-cuda=/usr/local/cuda --with-gdrcopy=/usr/local/gdrcopy --with-knem=/usr/local/knem --with-rdmacm --with-verbs --without-java
    make -j$(nproc)
    make -j$(nproc) install
    echo "/usr/local/ucx/lib" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
    rm -rf /var/tmp/ucx-1.8.0 /var/tmp/ucx-1.8.0.tar.gz
%environment
    export PATH=/usr/local/ucx/bin:$PATH
%post
    export PATH=/usr/local/ucx/bin:$PATH

# SLURM PMI2 version 18.08.8
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        bzip2 \
        file \
        make \
        perl \
        tar \
        wget
    rm -rf /var/lib/apt/lists/*
%post
    cd /
    mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://download.schedmd.com/slurm/slurm-18.08.8.tar.bz2
    mkdir -p /var/tmp && tar -x -f /var/tmp/slurm-18.08.8.tar.bz2 -C /var/tmp -j
    cd /var/tmp/slurm-18.08.8 &&   ./configure --prefix=/usr/local/slurm-pmi2
    cd /var/tmp/slurm-18.08.8
    make -C contribs/pmi2 install
    rm -rf /var/tmp/slurm-18.08.8 /var/tmp/slurm-18.08.8.tar.bz2

# OpenMPI version 4.0.4
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        bzip2 \
        file \
        hwloc \
        libnuma-dev \
        make \
        openssh-client \
        perl \
        tar \
        wget
    rm -rf /var/lib/apt/lists/*
%post
    cd /
    mkdir -p /var/tmp && wget -q -nc --no-check-certificate -P /var/tmp https://www.open-mpi.org/software/ompi/v4.0/downloads/openmpi-4.0.4.tar.bz2
    mkdir -p /var/tmp && tar -x -f /var/tmp/openmpi-4.0.4.tar.bz2 -C /var/tmp -j
    cd /var/tmp/openmpi-4.0.4 &&   ./configure --prefix=/usr/local/openmpi --disable-getpwuid --enable-mpi-cxx --enable-mpi-fortran --enable-mpi1-compatibility --enable-orterun-prefix-by-default --with-cuda --with-platform=contrib/platform/mellanox/optimized --with-pmi=/usr/local/slurm-pmi2 --with-ucx=/usr/local/ucx --without-verbs --without-xpmem
    make -j$(nproc)
    make -j$(nproc) install
    echo "/usr/local/openmpi/lib" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
    rm -rf /var/tmp/openmpi-4.0.4 /var/tmp/openmpi-4.0.4.tar.bz2
%environment
    export PATH=/usr/local/openmpi/bin:$PATH
%post
    export PATH=/usr/local/openmpi/bin:$PATH

BootStrap: docker
From: nvidia/cuda:10.1-base-ubuntu18.04
%post
    . /.singularity.d/env/10-docker*.sh

sed -i 's#archive.ubuntu.com/ubuntu/#free.nchc.org.tw/ubuntu#' /etc/apt/sources.list

# GNU compiler runtime
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        libgfortran3 \
        libgomp1
    rm -rf /var/lib/apt/lists/*

# Mellanox OFED version 4.7-3.2.9.0
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ca-certificates \
        gnupg \
        wget
    rm -rf /var/lib/apt/lists/*
%post
    wget -qO - https://www.mellanox.com/downloads/ofed/RPM-GPG-KEY-Mellanox | apt-key add -
    mkdir -p /etc/apt/sources.list.d && wget -q -nc --no-check-certificate -P /etc/apt/sources.list.d https://linux.mellanox.com/public/repo/mlnx_ofed/4.7-3.2.9.0/ubuntu18.04/mellanox_mlnx_ofed.list
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        ibverbs-utils \
        libibmad \
        libibmad-devel \
        libibumad \
        libibumad-devel \
        libibverbs-dev \
        libibverbs1 \
        libmlx4-1 \
        libmlx4-dev \
        libmlx5-1 \
        libmlx5-dev \
        librdmacm-dev \
        librdmacm1
    rm -rf /var/lib/apt/lists/*

# GDRCOPY
%files from build
    /usr/local/gdrcopy /usr/local/gdrcopy
%post
    cd /
    echo "/usr/local/gdrcopy/lib64" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
%environment
    export CPATH=/usr/local/gdrcopy/include:$CPATH
    export LIBRARY_PATH=/usr/local/gdrcopy/lib64:$LIBRARY_PATH
%post
    export CPATH=/usr/local/gdrcopy/include:$CPATH
    export LIBRARY_PATH=/usr/local/gdrcopy/lib64:$LIBRARY_PATH

# KNEM
%files from build
    /usr/local/knem /usr/local/knem
%post
    cd /
    echo "/usr/local/knem/lib" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
%environment
    export CPATH=/usr/local/knem/include:$CPATH
%post
    export CPATH=/usr/local/knem/include:$CPATH

# UCX
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        libbinutils
    rm -rf /var/lib/apt/lists/*
%files from build
    /usr/local/ucx /usr/local/ucx
%post
    cd /
    echo "/usr/local/ucx/lib" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
%environment
    export PATH=/usr/local/ucx/bin:$PATH
%post
    export PATH=/usr/local/ucx/bin:$PATH

# SLURM PMI2
%files from build
    /usr/local/slurm-pmi2 /usr/local/slurm-pmi2

# OpenMPI
%post
    apt-get update -y
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        hwloc \
        openssh-client
    rm -rf /var/lib/apt/lists/*
%files from build
    /usr/local/openmpi /usr/local/openmpi
%post
    cd /
    echo "/usr/local/openmpi/lib" >> /etc/ld.so.conf.d/hpccm.conf && ldconfig
%environment
    export PATH=/usr/local/openmpi/bin:$PATH
%post
    export PATH=/usr/local/openmpi/bin:$PATH

%environment
    export OMPI_MCA_pml=ucx
    export UCX_MEMTYPE_CACHE=n
    export UCX_TLS=rc,sm,cuda_copy,gdr_copy,cuda_ipc
%post
    export OMPI_MCA_pml=ucx
    export UCX_MEMTYPE_CACHE=n
    export UCX_TLS=rc,sm,cuda_copy,gdr_copy,cuda_ipc
